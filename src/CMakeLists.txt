cmake_minimum_required(VERSION 3.0.0)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/source)

add_library(Berig STATIC source/berig.cpp)

find_library(assimp assimp)

target_link_libraries(Berig assimp)
