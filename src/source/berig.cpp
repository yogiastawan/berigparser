#include <berig.h>
#include <iostream>
#include <fstream>
#include <filesystem>

#include <assimp/mesh.h>
#include <assimp/material.h>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

#include <GL/gl.h>

ether::Berig::Berig(/* args */)
{
}

ether::Berig::~Berig()
{
}

void ether::Berig::parserToOGL(std::string modelName, std::string outPath, bool createHeader) const
{
    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile(modelName, aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_GenUVCoords | aiProcess_TransformUVCoords | aiProcess_OptimizeMeshes | aiProcess_FlipUVs);
    if (scene == nullptr)
    {
        std::cout << "Cannot open file: " << modelName << std::endl;
        return;
    }

    ether::Model *model = new ether::Model();

    for (unsigned int i = 0; i < scene->mNumMeshes; i++)
    {
        std::cout << "Parse Mesh: " << i << std::endl;
        ether::Mesh *mesh = new ether::Mesh();
        // indice
        for (unsigned int j = 0; j < scene->mMeshes[i]->mNumFaces; j++)
        {
            mesh->indices.push_back(scene->mMeshes[i]->mFaces[j].mIndices[0]);
            mesh->indices.push_back(scene->mMeshes[i]->mFaces[j].mIndices[1]);
            mesh->indices.push_back(scene->mMeshes[i]->mFaces[j].mIndices[2]);
            std::cout << scene->mMeshes[i]->mFaces[j].mIndices[0] << "," << scene->mMeshes[i]->mFaces[j].mIndices[1] << "," << scene->mMeshes[i]->mFaces[j].mIndices[2] << std::endl;
            mesh->drawCount += 3;
        }

        // vertice
        for (unsigned int j = 0; j < scene->mMeshes[i]->mNumVertices; j++)
        {
            mesh->vertices.push_back(glm::vec3(scene->mMeshes[i]->mVertices[j].x, scene->mMeshes[i]->mVertices[j].y, scene->mMeshes[i]->mVertices[j].z));
            // uvs
            if (scene->mMeshes[i]->mTextureCoords[0] != nullptr)
            {
                mesh->uvs.push_back(glm::vec2(scene->mMeshes[i]->mTextureCoords[0][j].x, scene->mMeshes[i]->mTextureCoords[0][j].y));
            }
        }

        if (mesh->uvs.size() > 0)
        {
            mesh->hasUvs = true;
        }

        mesh->materialIndex = scene->mMeshes[i]->mMaterialIndex;

        // add to model
        model->meshes.push_back(mesh);
    }

    for (unsigned int i = 0; i < scene->mNumMaterials; i++)
    {
        std::cout << "Parse Material: " << i << std::endl;
        ether::Material *material = new ether::Material();
        aiColor3D c(1.0f, 1.0f, 1.0f);
        // scene->mMaterials[i]->Get(AI_MATKEY_COLOR_DIFFUSE, material->diffuseColor);
        scene->mMaterials[i]->Get(AI_MATKEY_COLOR_DIFFUSE, c);
        std::cout << "Diffuse color: " << c.r << ", " << c.g << ", " << c.b << std::endl;
        material->diffuseColor = glm::vec3(c.r, c.g, c.b);
        if (scene->mMaterials[i]->GetTextureCount(aiTextureType_DIFFUSE) > 0)
        {
            material->hasDiffuseTexture = true;
            aiString fn;
            scene->mMaterials[i]->GetTexture(aiTextureType_DIFFUSE, 0, &fn);
            material->fileTexture = std::string(fn.C_Str());
        }
        model->materials.push_back(material);
    }

    // free scene
    importer.FreeScene();

    std::string fileOutName = getFileName(modelName);
    std::cout << "File Name: " << fileOutName << std::endl;
    // write to file
    if (createHeader)
    {
        createHeaderFile(outPath);
    }

    writeBin(model, outPath.append("/").append(fileOutName).append(".emd"));

    // free all temp data
    for (ether::Mesh *m : model->meshes)
    {
        m->indices.clear();
        m->vertices.clear();
        m->uvs.clear();
        delete m;
    }
    model->meshes.clear();

    for (ether::Material *m : model->materials)
    {
        m->fileTexture.clear();
    }
    model->materials.clear();

    delete model;
}

void ether::Berig::createHeaderFile(std::string out) const
{
    std::ofstream outFile;
    outFile.open(out.append("/model.h"));
    std::string txt = R"(#ifndef __ETHER_MODEL_H__
#define __ETHER_MODEL_H__

#define GL_GLEXT_PROTOTYPES
#include <iostream>
#include <vector>
#include <glm/glm.hpp>

#include <GL/gl.h>

#include <SDL.h>

namespace ether
{
	struct Mesh
	{
		unsigned int drawCount = 0;
		unsigned int materialIndex = 0;
		unsigned int indexBuffer;
		unsigned int uvBuffer;
		unsigned int vertexBuffer;
		unsigned int vaoBuffer;
		bool hasUvs = false;
		std::vector<glm::vec3> vertices;
		std::vector<unsigned int> indices;
		std::vector<glm::vec2> uvs;
	};
	struct Material
	{
		glm::vec3 diffuseColor;
		unsigned int diffuseTexture;
		bool hasDiffuseTexture;
		std::string fileTexture;
	};
	struct Model
	{
		std::vector<Mesh *> meshes;
		std::vector<Material *> materials;
		void load(GLenum usage, GLuint vertexAttrib, GLuint uvAttrib)
		{
			for (ether::Mesh *m : meshes)
			{
				// genereate index buffer
				glGenBuffers(1, &m->indexBuffer);
				glBindBuffer(GL_ARRAY_BUFFER, m->indexBuffer);
				glBufferData(GL_ARRAY_BUFFER, m->indices.size() * sizeof(unsigned int), &m->indices[0], usage);

				// generate vertext buffer
				glGenBuffers(1, &m->vertexBuffer);
				glBindBuffer(GL_ARRAY_BUFFER, m->vertexBuffer);
				glBufferData(GL_ARRAY_BUFFER, m->vertices.size() * sizeof(glm::vec3), &m->vertices[0], usage);

				// generate uv buffer
				if (m->hasUvs)
				{
					glGenBuffers(1, &m->uvBuffer);
					glBindBuffer(GL_ARRAY_BUFFER, m->uvBuffer);
					glBufferData(GL_ARRAY_BUFFER, m->uvs.size() * sizeof(glm::vec2), &m->uvs[0], usage);
					glEnableVertexAttribArray(uvAttrib);
					glVertexAttribPointer(uvAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
				}

				// generate vao
				glGenVertexArrays(1, &m->vaoBuffer);
				glBindVertexArray(m->vaoBuffer);

				// vertex buffer
				glBindBuffer(GL_ARRAY_BUFFER, m->vertexBuffer);
				glEnableVertexAttribArray(vertexAttrib);
				glVertexAttribPointer(vertexAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);

				// uv buffer
				if (m->hasUvs)
				{
					glBindBuffer(GL_ARRAY_BUFFER, m->uvBuffer);
					glEnableVertexAttribArray(uvAttrib);
					glVertexAttribPointer(uvAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
				}

				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m->indexBuffer);

				// Unbind everything
				glBindVertexArray(0);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);
			}

			for (ether::Material *m : materials)
			{
				SDL_Surface *texture = nullptr;
				texture = IMG_Load(m->fileTexture.c_str());

				if (texture != nullptr)
				{
					glBindTexture(GL_TEXTURE_2D, m->diffuseTexture);

					// Textures have to be passed to OpenGL in the right way depending on format. This is by no means a complete list, and some texture formats might still fail.
					switch (texture->format->format)
					{
					case SDL_PIXELFORMAT_RGB24:
					case SDL_PIXELFORMAT_RGB332:
					case SDL_PIXELFORMAT_RGB444:
					case SDL_PIXELFORMAT_RGB555:
					case SDL_PIXELFORMAT_RGB565:
					case SDL_PIXELFORMAT_RGB888:
						// RGB format
						glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB, texture->w, texture->h, 0, GL_RGB, GL_UNSIGNED_BYTE, texture->pixels);
						break;
					case SDL_PIXELFORMAT_RGBA4444:
					case SDL_PIXELFORMAT_RGBA5551:
					case SDL_PIXELFORMAT_RGBA8888:
						// RGBA format
						glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB_ALPHA, texture->w, texture->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture->pixels);
						break;
					case SDL_PIXELFORMAT_BGR24:
					case SDL_PIXELFORMAT_BGR555:
					case SDL_PIXELFORMAT_BGR565:
					case SDL_PIXELFORMAT_BGR888:
						// BGR format
						glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB, texture->w, texture->h, 0, GL_BGR, GL_UNSIGNED_BYTE, texture->pixels);
						break;
					case SDL_PIXELFORMAT_ABGR1555:
					case SDL_PIXELFORMAT_ABGR4444:
					case SDL_PIXELFORMAT_ABGR8888:
						glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB_ALPHA, texture->w, texture->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture->pixels);
						break;
					case SDL_PIXELFORMAT_ARGB1555:
					case SDL_PIXELFORMAT_ARGB2101010:
					case SDL_PIXELFORMAT_ARGB4444:
					case SDL_PIXELFORMAT_ARGB8888:
						glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB_ALPHA, texture->w, texture->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture->pixels);
						break;
					default:
						std::cout << "Unknown texture format: " << SDL_GetPixelFormatName(texture->format->format) << std::endl;
						break;
					}

					// Enable mipmapping
					glGenerateMipmap(GL_TEXTURE_2D);

					// Set texture parameters.
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);					// Repeat wrapping
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);					// Repeat wrapping
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);				// linear mag filtering
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // trilinear min filtering

					// Unbind the texture
					glBindTexture(GL_TEXTURE_2D, 0);

					std::cout << "Loaded texture: " << m->fileTexture << std::endl;
				}
				else
				{
					std::cout << "Failed to load texture: " << m->fileTexture << std::endl;
				}

				SDL_FreeSurface(texture);
				texture = nullptr;
			}
		}

		void draw(GLuint diffuseColorU, GLuint hasDiffuseTextureU, GLuint diffuseTextureU)
		{
			for (ether::Mesh *m : meshes)
			{
				// Get material to draw with
				ether::Material *mat = materials[m->materialIndex];
				// Update material uniforms
				glUniform3fv(diffuseColorU, 1, &material->diffuseColor[0]);
				glUniform1i(hasDiffuseTextureU, material->hasDiffuseTexture);

				// Use texture if material has diffuse texture
				if (mat->hasDiffuseTexture)
				{
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, mat->diffuseTexture);
					glUniform1i(hasDiffuseTextureU, 0);
				}

				// Draw
				glBindVertexArray(m->vertexArrayObject);
				glDrawElements(GL_TRIANGLES, m->drawCount, GL_UNSIGNED_INT, 0);
			}
		}
	};
}
#endif //__ETHER_MODEL_H__)";

    outFile << txt;
    txt.clear();
    outFile.close();
}

std::string ether::Berig::getFileName(const std::string &s) const
{

    char sep = '/';

#ifdef _WIN32
    sep = '\\';
#endif

    size_t i = s.rfind(sep, s.length());
    size_t j = s.rfind('.', s.length());
    if (i != std::string::npos)
    {
        // return (s.substr(i + 1, s.length() - i));
        return (s.substr(i + 1, j - 1 - i));
    }

    return ("");
}

void ether::Berig::writeBin(const ether::Model *model, std::string outPath) const
{
    // open file output
    std::ofstream fileOut;
    fileOut.open(outPath, std::ios::binary);
    // clear name file holder
    outPath.clear();
    if (!fileOut)
    {
        std::cout << "Failed create file" << std::endl;
        fileOut.close();
        return;
    }

    // write type file
    const char *typeFile = "%emd";
    fileOut.write(typeFile, sizeof(char) * 4);

    // write data to file output
    // fileOut.write((char *)model, sizeof(ether::Model));
    unsigned int meshSize = model->meshes.size();
    std::cout << "Found Meshes: " << meshSize << std::endl;
    unsigned int materialSize = model->materials.size();
    std::cout << "Found Material: " << materialSize << std::endl;

    // write metadata bin file
    // write model mesh size
    fileOut.write((char *)&meshSize, sizeof(unsigned int));
    // write model material size
    fileOut.write((char *)&materialSize, sizeof(unsigned int));
    // write size indices in each mesh
    for (ether::Mesh *m : model->meshes)
    {
        unsigned int sze = m->indices.size();
        std::cout << "Size indice: " << sze << std::endl;
        fileOut.write((char *)&sze, sizeof(unsigned int));
    }

    // write size vertices in each mesh
    for (ether::Mesh *m : model->meshes)
    {
        unsigned int sze = m->vertices.size();
        std::cout << "Size vertice: " << sze << std::endl;
        fileOut.write((char *)&sze, sizeof(unsigned int));
    }

    // write size uvs in each mesh
    for (ether::Mesh *m : model->meshes)
    {
        unsigned int sze = m->uvs.size();
        std::cout << "Size uv: " << sze << std::endl;
        fileOut.write((char *)&sze, sizeof(unsigned int));
    }

    // write length of name file texture in each material
    for (ether::Material *m : model->materials)
    {
        // m->fileTexture = std::string("Hiii"); // just test
        unsigned int sze = m->fileTexture.length();
        std::cout << "Length file name: " << sze << std::endl;
        fileOut.write((char *)&sze, sizeof(unsigned int));
    }

    // write mesh data
    for (ether::Mesh *m : model->meshes)
    {
        fileOut.write((char *)&(m->drawCount), sizeof(unsigned int));
        fileOut.write((char *)&(m->materialIndex), sizeof(unsigned int));
        // fileOut.write((char *)&(m->indexBuffer), sizeof(unsigned int));
        // fileOut.write((char *)&(m->vertexBuffer), sizeof(unsigned int));
        // fileOut.write((char *)&(m->uvBuffer), sizeof(unsigned int));
        // fileOut.write((char *)&(m->vaoBuffer), sizeof(unsigned int));
        fileOut.write((char *)&(m->hasUvs), sizeof(bool));
        fileOut.write((char *)m->indices.data(), sizeof(unsigned int) * m->indices.size());
        fileOut.write((char *)m->vertices.data(), sizeof(glm::vec3) * m->vertices.size());
        fileOut.write((char *)m->uvs.data(), sizeof(glm::vec2) * m->uvs.size());
    }

    // write texture data
    for (ether::Material *m : model->materials)
    {
        std::cout << "Diffuse color: " << m->diffuseColor.x << ", " << m->diffuseColor.y << ", " << m->diffuseColor.z << std::endl;
        fileOut.write((char *)&m->diffuseColor, sizeof(glm::vec3));
        std::cout << "Has Diffuse Texture: " << m->hasDiffuseTexture << std::endl;
        fileOut.write((char *)&m->hasDiffuseTexture, sizeof(bool));
        std::cout << "File Texture: " << m->fileTexture << "=>" << m->fileTexture.length() << std::endl;
        fileOut.write(m->fileTexture.c_str(), sizeof(char) * m->fileTexture.length());
    }

    // close file
    fileOut.close();
}