#ifndef __ETHER_BERIG_H__
#define __ETHER_BERIG_H__

#include <iostream>
#include <model.h>

namespace ether
{

    class Berig
    {
    private:
        /* data */
        std::string getFileName(const std::string &s) const;
        void createHeaderFile(std::string out) const;
        void writeBin(const ether::Model *model, std::string outPath) const;

    public:
        Berig(/* args */);
        ~Berig();
        void parserToOGL(std::string fileName, std::string outPath, bool createHeader) const;
        // void parserToOGLCpp(std::string modelName, std::string outPath, bool createHeader) const;
    };
}

#endif //__ETHER_BERIG_H__