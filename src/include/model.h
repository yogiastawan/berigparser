#ifndef __ETHER_MODEL_H__
#define __ETHER_MODEL_H__

#include <vector>
#include <iostream>
#include <glm/glm.hpp>

namespace ether
{
    struct Mesh
    {
        unsigned int drawCount = 0;
        unsigned int materialIndex = 0;
        unsigned int indexBuffer;
        unsigned int vertexBuffer;
        unsigned int uvBuffer;
        unsigned int vaoBuffer;
        bool hasUvs = false;

        std::vector<unsigned int> indices;
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec2> uvs;
    };

    struct Material
    {
        glm::vec3 diffuseColor;
        unsigned int diffuseTexture;
        bool hasDiffuseTexture;

        std::string fileTexture;
    };

    struct Model
    {
        // unsigned int meshSize;
        // unsigned int materialSize;

        std::vector<Mesh *> meshes;
        std::vector<Material *> materials;
    };
}

#endif //__ETHER_MODEL_H__