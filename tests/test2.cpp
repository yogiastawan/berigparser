#include <model.h>

#include <iostream>
#include <fstream>
#include <vector>

#include <glm/glm.hpp>

int main(int argc, char const *argv[])
{
    std::cout << "Load file binary: " << argv[1] << std::endl;
    ether::Model a;

    std::ifstream in;
    in.open(argv[1], std::ios::binary | std::ios::in);

    if (!in)
    {
        std::cout << "Cannot open file";
        return 1;
    }

    // read type file
    char *typeFile = new char[4];
    in.read(typeFile, sizeof(char) * 4);

    std::cout << "Type: " << typeFile << std::endl;
    std::string tf = std::string(typeFile);
    delete[] typeFile;
    if (tf.compare("%emd") != 0)
    {
        std::cout << "File type: "<<tf<<" is not valid" << std::endl;
    }

    // read numb of mesh(s) and material(s)
    unsigned int sizeMesh;
    unsigned int sizeMaterial;
    in.read((char *)&sizeMesh, sizeof(unsigned int));
    in.read((char *)&sizeMaterial, sizeof(unsigned int));

    std::cout << "size of mesh: " << sizeMesh << std::endl;
    std::cout << "size of material: " << sizeMaterial << std::endl;

    // read meta data
    unsigned int *sizeIndiceMesh = new unsigned int[sizeMesh];
    unsigned int *sizeVerticeMesh = new unsigned int[sizeMesh];
    unsigned int *sizeUvMesh = new unsigned int[sizeMesh];
    in.read((char *)sizeIndiceMesh, sizeof(unsigned int) * sizeMesh);
    in.read((char *)sizeVerticeMesh, sizeof(unsigned int) * sizeMesh);
    in.read((char *)sizeUvMesh, sizeof(unsigned int) * sizeMesh);

    unsigned int *lengthFileTextureMaterial = new unsigned int[sizeMaterial];
    in.read((char *)lengthFileTextureMaterial, sizeof(unsigned int) * sizeMaterial);

    for (unsigned int i = 0; i < sizeMesh; i++)
    {
        std::cout << "Mesh " << i << " indices size: " << sizeIndiceMesh[i] << std::endl;
        std::cout << "Mesh " << i << " vertices size: " << sizeVerticeMesh[i] << std::endl;
        std::cout << "Mesh " << i << " uvs size: " << sizeUvMesh[i] << std::endl;

        unsigned int drawCount;
        in.read((char *)&drawCount, sizeof(unsigned int));
        std::cout << "Mesh " << i << " draw count: " << drawCount << std::endl;
        unsigned int materialIndex;
        in.read((char *)&materialIndex, sizeof(unsigned int));
        std::cout << "Mesh " << i << " material index: " << materialIndex << std::endl;
        unsigned int indexBuffer;
        in.read((char *)&indexBuffer, sizeof(unsigned int));
        std::cout << "Mesh " << i << " index buffer: " << indexBuffer << std::endl;
        unsigned int vertexBuffer;
        in.read((char *)&vertexBuffer, sizeof(unsigned int));
        std::cout << "Mesh " << i << " vertex buffer: " << vertexBuffer << std::endl;
        unsigned int uvBuffer;
        in.read((char *)&uvBuffer, sizeof(unsigned int));
        std::cout << "Mesh " << i << " uv buffer: " << uvBuffer << std::endl;
        unsigned int vaoBuffer;
        in.read((char *)&vaoBuffer, sizeof(unsigned int));
        std::cout << "Mesh " << i << " vao buffer: " << vaoBuffer << std::endl;
        unsigned int hasUvs;
        in.read((char *)&hasUvs, sizeof(bool));
        std::cout << "Mesh " << i << " has uvs: " << hasUvs << std::endl;
        std::cout << "Mesh " << i << " indices size: " << sizeIndiceMesh[i] << std::endl;
        unsigned int *indices = new unsigned int[sizeIndiceMesh[i]];
        in.read((char *)indices, sizeof(unsigned int) * sizeIndiceMesh[i]);
        for (unsigned int j = 0; j < 36; j++)
        {
            std::cout << indices[j] << " ";
        }
        delete[] indices;
        std::cout << std::endl;

        glm::vec3 *vertices = new glm::vec3[sizeVerticeMesh[i]];
        in.read((char *)vertices, sizeof(glm::vec3) * sizeVerticeMesh[i]);
        for (unsigned int j = 0; j < sizeVerticeMesh[i]; j++)
        {
            std::cout << vertices[j].x << ", " << vertices[j].y << ", " << vertices[j].z << " || ";
        }
        delete[] vertices;
        std::cout << std::endl;

        glm::vec2 *uvs = new glm::vec2[sizeUvMesh[i]];
        in.read((char *)uvs, sizeof(glm::vec2) * sizeUvMesh[i]);
        for (unsigned int j = 0; j < sizeUvMesh[i]; j++)
        {
            std::cout << uvs[j].x << ", " << uvs[j].y << " || ";
        }
        delete[] uvs;
        std::cout << std::endl;
    }

    // material
    for (unsigned int i = 0; i < sizeMaterial; i++)
    {
        glm::vec3 diffuseColor;
        in.read((char *)&diffuseColor, sizeof(glm::vec3));
        std::cout << "Material " << i << " diffuse color: " << diffuseColor.x << ", " << diffuseColor.y << ", " << diffuseColor.z << std::endl;
        unsigned int diffuseTexture;
        in.read((char *)&diffuseTexture, sizeof(unsigned int));
        std::cout << "Material " << i << " diffuse texture: " << diffuseTexture << std::endl;
        char *fileTexture = new char[lengthFileTextureMaterial[i]];
        in.read(fileTexture, sizeof(char) * lengthFileTextureMaterial[i]);
        std::cout << "Material " << i << " file texture: " << std::string(fileTexture) << std::endl;
        delete[] fileTexture;
    }

    in.close();

    delete[] sizeIndiceMesh;
    delete[] sizeVerticeMesh;
    delete[] sizeUvMesh;
    delete[] lengthFileTextureMaterial;
    return 0;
}
