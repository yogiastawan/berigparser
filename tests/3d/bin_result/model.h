#ifndef __ETHER_MODEL_H__
#define __ETHER_MODEL_H__

#define GL_GLEXT_PROTOTYPES
#include <iostream>
#include <vector>
#include <glm/glm.hpp>

#include <GL/gl.h>

#include <SDL.h>

namespace ether
{
	struct Mesh
	{
		unsigned int drawCount = 0;
		unsigned int materialIndex = 0;
		unsigned int indexBuffer;
		unsigned int uvBuffer;
		unsigned int vertexBuffer;
		unsigned int vaoBuffer;
		bool hasUvs = false;
		std::vector<glm::vec3> vertices;
		std::vector<unsigned int> indices;
		std::vector<glm::vec2> uvs;
	};
	struct Material
	{
		glm::vec3 diffuseColor;
		unsigned int diffuseTexture;
		bool hasDiffuseTexture;
		std::string fileTexture;
	};
	struct Model
	{
		std::vector<Mesh *> meshes;
		std::vector<Material *> materials;
		void load(GLenum usage, GLuint vertexAttrib, GLuint uvAttrib)
		{
			for (ether::Mesh *m : meshes)
			{
				// genereate index buffer
				glGenBuffers(1, &m->indexBuffer);
				glBindBuffer(GL_ARRAY_BUFFER, m->indexBuffer);
				glBufferData(GL_ARRAY_BUFFER, m->indices.size() * sizeof(unsigned int), &m->indices[0], usage);

				// generate vertext buffer
				glGenBuffers(1, &m->vertexBuffer);
				glBindBuffer(GL_ARRAY_BUFFER, m->vertexBuffer);
				glBufferData(GL_ARRAY_BUFFER, m->vertices.size() * sizeof(glm::vec3), &m->vertices[0], usage);

				// generate uv buffer
				if (m->hasUvs)
				{
					glGenBuffers(1, &m->uvBuffer);
					glBindBuffer(GL_ARRAY_BUFFER, m->uvBuffer);
					glBufferData(GL_ARRAY_BUFFER, m->uvs.size() * sizeof(glm::vec2), &m->uvs[0], usage);
					glEnableVertexAttribArray(uvAttrib);
					glVertexAttribPointer(uvAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
				}

				// generate vao
				glGenVertexArrays(1, &m->vaoBuffer);
				glBindVertexArray(m->vaoBuffer);

				// vertex buffer
				glBindBuffer(GL_ARRAY_BUFFER, m->vertexBuffer);
				glEnableVertexAttribArray(vertexAttrib);
				glVertexAttribPointer(vertexAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);

				// uv buffer
				if (m->hasUvs)
				{
					glBindBuffer(GL_ARRAY_BUFFER, m->uvBuffer);
					glEnableVertexAttribArray(uvAttrib);
					glVertexAttribPointer(uvAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
				}

				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m->indexBuffer);

				// Unbind everything
				glBindVertexArray(0);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);
			}

			for (ether::Material *m : materials)
			{
				SDL_Surface *texture = nullptr;
				texture = IMG_Load(m->fileTexture.c_str());

				if (texture != nullptr)
				{
					glBindTexture(GL_TEXTURE_2D, m->diffuseTexture);

					// Textures have to be passed to OpenGL in the right way depending on format. This is by no means a complete list, and some texture formats might still fail.
					switch (texture->format->format)
					{
					case SDL_PIXELFORMAT_RGB24:
					case SDL_PIXELFORMAT_RGB332:
					case SDL_PIXELFORMAT_RGB444:
					case SDL_PIXELFORMAT_RGB555:
					case SDL_PIXELFORMAT_RGB565:
					case SDL_PIXELFORMAT_RGB888:
						// RGB format
						glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB, texture->w, texture->h, 0, GL_RGB, GL_UNSIGNED_BYTE, texture->pixels);
						break;
					case SDL_PIXELFORMAT_RGBA4444:
					case SDL_PIXELFORMAT_RGBA5551:
					case SDL_PIXELFORMAT_RGBA8888:
						// RGBA format
						glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB_ALPHA, texture->w, texture->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture->pixels);
						break;
					case SDL_PIXELFORMAT_BGR24:
					case SDL_PIXELFORMAT_BGR555:
					case SDL_PIXELFORMAT_BGR565:
					case SDL_PIXELFORMAT_BGR888:
						// BGR format
						glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB, texture->w, texture->h, 0, GL_BGR, GL_UNSIGNED_BYTE, texture->pixels);
						break;
					case SDL_PIXELFORMAT_ABGR1555:
					case SDL_PIXELFORMAT_ABGR4444:
					case SDL_PIXELFORMAT_ABGR8888:
						glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB_ALPHA, texture->w, texture->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture->pixels);
						break;
					case SDL_PIXELFORMAT_ARGB1555:
					case SDL_PIXELFORMAT_ARGB2101010:
					case SDL_PIXELFORMAT_ARGB4444:
					case SDL_PIXELFORMAT_ARGB8888:
						glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB_ALPHA, texture->w, texture->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture->pixels);
						break;
					default:
						std::cout << "Unknown texture format: " << SDL_GetPixelFormatName(texture->format->format) << std::endl;
						break;
					}

					// Enable mipmapping
					glGenerateMipmap(GL_TEXTURE_2D);

					// Set texture parameters.
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);					// Repeat wrapping
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);					// Repeat wrapping
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);				// linear mag filtering
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // trilinear min filtering

					// Unbind the texture
					glBindTexture(GL_TEXTURE_2D, 0);

					std::cout << "Loaded texture: " << m->fileTexture << std::endl;
				}
				else
				{
					std::cout << "Failed to load texture: " << m->fileTexture << std::endl;
				}

				SDL_FreeSurface(texture);
				texture = nullptr;
			}
		}

		void draw(GLuint diffuseColorU, GLuint hasDiffuseTextureU, GLuint diffuseTextureU)
		{
			for (ether::Mesh *m : meshes)
			{
				// Get material to draw with
				ether::Material *mat = materials[m->materialIndex];
				// Update material uniforms
				glUniform3fv(diffuseColorU, 1, &material->diffuseColor[0]);
				glUniform1i(hasDiffuseTextureU, material->hasDiffuseTexture);

				// Use texture if material has diffuse texture
				if (mat->hasDiffuseTexture)
				{
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, mat->diffuseTexture);
					glUniform1i(hasDiffuseTextureU, 0);
				}

				// Draw
				glBindVertexArray(m->vertexArrayObject);
				glDrawElements(GL_TRIANGLES, m->drawCount, GL_UNSIGNED_INT, 0);
			}
		}
	};
}
#endif //__ETHER_MODEL_H__