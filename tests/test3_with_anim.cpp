#include <berig.h>

int main(int argc, char const *argv[])
{
    std::cout<<"Load file: "<<argv[1]<<std::endl;
    const ether::Berig a;
    a.parserToOGL(argv[1],argv[2],true);
    return 0;
}
